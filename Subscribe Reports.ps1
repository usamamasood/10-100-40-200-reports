[CmdletBinding()]
Param(
	[parameter(Mandatory=$true)]
	[string] $reportServerName,
	[parameter(Mandatory=$true)]
	[string] $fromDirectory,
	[parameter(Mandatory=$true)]
	[string] $serverPath,
	[parameter(Mandatory=$true)]
	[string] $dataSourceDbServer,
	[parameter(Mandatory=$true)]
	[string] $dataSourceDb,
	[parameter(Mandatory=$true)]
	[string] $dataSourceDbUserName,
	[parameter(Mandatory=$true)]
	[string] $dataSourceDbPassword,
	[parameter(Mandatory=$true)]
	[string] $vmAdminUserName,
	[parameter(Mandatory=$true)]
	[string] $vmAdminPassword
)
$ReportName = 'SynertrexReport'

Write-Output "Connecting to $reportServerName"
$reportServerUri = "http://{0}/ReportServer/ReportService2010.asmx" -f $reportServerName
$vmAdminPasswordSecured = ConvertTo-SecureString $vmAdminPassword -AsPlainText -Force -Verbose
$SsrsVmCreds = New-Object System.Management.Automation.PSCredential ($vmAdminUserName, $vmAdminPasswordSecured)
$proxy = New-WebServiceProxy -Uri $reportServerUri -Credential $SsrsVmCreds
$proxyNamespace = $proxy.GetType().Namespace
Write-Output "Connected to $reportServerName"

# Set the extension setting as report server email.
Write-Output "Creating dilevery extension alongwith required parameters"
$settings = New-Object ("$proxyNamespace.ExtensionSettings")
$settings.Extension = "Report Server FileShare";

# Set the extension parameter values.

$extensionParams = @()
      
$fileName	 = New-Object ("$proxyNamespace.ParameterFieldReference") # Data-driven.
$fileName.ParameterName = "FILENAME"
$fileName.FieldAlias = "ReportName" 
$extensionParams += $fileName


$path = New-Object ("$proxyNamespace.ParameterValue")
$path.Name = "PATH";
$path.Value ="\\us153fdstore17.file.core.windows.net\reports"; # This is currently fixed. It need to be reffered from dataset for production server requirements.
$extensionParams += $path;

$renderFormat = New-Object ("$proxyNamespace.ParameterFieldReference")             
$renderFormat.ParameterName = "RENDER_FORMAT";
$renderFormat.FieldAlias = "RenderFormat";
$extensionParams += $renderFormat;

$writeMode = New-Object ("$proxyNamespace.ParameterFieldReference")
$writeMode.ParameterName = "WRITEMODE"
$writeMode.FieldAlias = "WriteMode"
$extensionParams += $writeMode

$fileExtension = New-Object ("$proxyNamespace.ParameterFieldReference")
$fileExtension.ParameterName = "FILEEXTN";
$fileExtension.FieldAlias = "FileExtension"
$extensionParams += $fileExtension;

$useFileShareAccount = New-Object ("$proxyNamespace.ParameterFieldReference")    
$useFileShareAccount.ParameterName = "DEFAULTCREDENTIALS";
$useFileShareAccount.FieldAlias = "UseFileShareAccount";
$extensionParams += $useFileShareAccount;

$settings.ParameterValues = $extensionParams;
Write-Output "Dilevery extension created"

# Create the data source for the delivery query.
Write-Output "Creating Data Source for dilevery query"
$delivery = New-Object ("$proxyNamespace.DataSource") 
$delivery.Name = "";

# Connecting to data source
#$dataSourceDbServer = "$dataSourceDbServer "
#$dataSourceDb = "$dataSourceDb"
#$dataSourceDbUserName = "$dataSourceDbUserName"
#$dataSourceDbPassword = "$dataSourceDbPassword"
$dataSourceDefinition = New-Object ("$proxyNamespace.DataSourceDefinition") 
$dataSourceDefinition.ConnectString  = "Data Source=$($dataSourceDbServer).database.windows.net;Initial Catalog=$($dataSourceDb);Encrypt=True;TrustServerCertificate=False"
$dataSourceDefinition.Extension  = "SQL"
$dataSourceDefinition.UserName = $dataSourceDbUserName
$dataSourceDefinition.Password = $dataSourceDbPassword
$dataSourceDefinition.WindowsCredentials  = $false
$dataSourceDefinition.CredentialRetrieval = "Store"
$delivery.Item = $dataSourceDefinition;

Write-Output "Dilevery Source created"

# Create the fields list.      
Write-Output "Creating dilevery Fields"
$fieldsList = @()

$field = New-Object ("$proxyNamespace.Field")
$field.Name = "ReportName"
$field.Alias = "ReportName"
$fieldsList += $field

$field = New-Object ("$proxyNamespace.Field")
$field.Name = "Path"
$field.Alias = "Path"
$fieldsList += $field

$field = New-Object ("$proxyNamespace.Field")
$field.Name = "RenderFormat"
$field.Alias = "RenderFormat"
$fieldsList += $field

$field = New-Object ("$proxyNamespace.Field")
$field.Name = "WriteMode"
$field.Alias = "WriteMode"
$fieldsList += $field

$field = New-Object ("$proxyNamespace.Field")
$field.Name = "FileExtension"
$field.Alias = "FileExtension"
$fieldsList += $field

$field = New-Object ("$proxyNamespace.Field")
$field.Name = "UseFileShareAccount"
$field.Alias = "UseFileShareAccount"
$fieldsList += $field
Write-Output "Dilevery Fields created"

# Create the data set for the delivery query.
Write-Output "Creating the data set for the delivery query"
$dataSetDefinition = New-Object ("$proxyNamespace.DataSetDefinition")      
$dataSetDefinition.AccentSensitivitySpecified = $false;
$dataSetDefinition.CaseSensitivitySpecified = $false;
$dataSetDefinition.KanatypeSensitivitySpecified = $false;
$dataSetDefinition.WidthSensitivitySpecified = $false;
$dataSetDefinition.Fields = $fieldsList;
      
$queryDefinition = New-Object ("$proxyNamespace.QueryDefinition")      
$queryDefinition.CommandText = "SELECT TOP 1 ReportName, Path, RenderFormat, WriteMode, FileExtension, UseFileShareAccount FROM Reports.ReportSubscriptionConfiguration"
$queryDefinition.CommandType = "Text";
$queryDefinition.Timeout = 45;
$queryDefinition.TimeoutSpecified = $true;
$dataSetDefinition.Query = $queryDefinition;

$results = New-Object ("$proxyNamespace.DataSetDefinition")          
$results = $proxy.PrepareQuery($delivery, $dataSetDefinition, [ref]$null, [ref]$null)

Write-Output "Data set created for the delivery query"

#Creating schedular
Write-Output "Setting up a schedular"
$dataRetrieval = New-Object ("$proxyNamespace.DataRetrievalPlan")            
$dataRetrieval.DataSet = $results;
$dataRetrieval.Item = $dataSourceDefinition;
# Set the event type and match data for the delivery.
$eventType = "TimedSubscription";
$matchData = "<ScheduleDefinition><StartDateTime>2018-04-17T13:20:00-00:00</StartDateTime><WeeklyRecurrence><WeeksInterval>1</WeeksInterval><DaysOfWeek><Monday>True</Monday><Tuesday>True</Tuesday><Wednesday>True</Wednesday><Thursday>True</Thursday><Friday>True</Friday></DaysOfWeek></WeeklyRecurrence></ScheduleDefinition>";
Write-Output "schedular created"

Write-Output "Creating subscription"
$files = @(get-childitem $fromDirectory * -rec|where-object {!($_.psiscontainer)})
$reports = @($files | where-object { [System.IO.Path]::GetExtension($_) -eq ".rdl" })
$reportPath = $serverPath + '/' + $ReportName
$subscriptionID = $proxy.CreateDataDrivenSubscription($reportPath, $settings, $dataRetrieval, $description, $eventType, $matchData, $parameters)

Write-Output "Subscription created"
